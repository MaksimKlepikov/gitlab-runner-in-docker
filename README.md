   
docker run -it --name gitlab-runner gitlab-runner ash

    gitlab-runner register --url https://gitlab.com \
        --registration-token [YOUR_TOKEN] \
        --executor "docker" \
        --name "runner" \
        --docker-image "alpine:latest"
        
or

    gitlab-runner run-single \
        -u http://gitlab.com -t [RUNNER_TOCKET] \
        --executor docker \
        --docker-image alpine

next ctrl + p ctrl + q
